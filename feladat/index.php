<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<head>
    <script type="text/javascript" src="src/resources/js/index.js"></script>
    <script type="text/javascript" src="src/resources/js/main-page.js"></script>
    <script type="text/javascript" src="src/resources/js/statistics.js"></script>
    <script type="text/javascript" src="src/resources/js/add-news.js"></script>
    <link rel="stylesheet" type="text/css" href="src/resources/css/style.css">
    <link rel="shortcut icon" href="src/resources/css/img/icon.png"/>
    <title>Mini-News</title>

</head>

<body>
<header>
    <h1 id="header-text"></h1>
</header>
<div id="main-div" class="content-div">

    <div id="create-news" class="form post">
        <img id="logo" class="logo" src="src/resources/css/img/icon.png">
        <p>Title:</p>
        <input type="text" id="title" class="text-input">
        <p>Content:</p>
        <textarea class="text-area" id="content"></textarea>
        <p>Tags</p>
        <input type="text" id="tags" class="text-input">
        <br>
        <button class="btn" onclick="onCreateButtonClick()">Create</button>
    </div>

    <div class="form filter">
        <div class="elements">
            Options:
            <div class="button-div">
                <button class="btn btn-stat" onclick="onOrderByDateAscClick()">Order news by date ascending</button>
                <br>
                <button class="btn btn-stat" onclick="onOrderByDateDescClick()">Order news by date descending</button>
                <br>
                <button class="btn btn-stat" onclick="onNewsWithMostWordsClick()">News with most words</button>
                <br>
                <button class="btn btn-stat" onclick="onAverageNewsLengthClick()">Average news length</button>
                <br>
                <button class="btn btn-stat" onclick="onMostUsedTagsClick()">Top 10 most used tags</button>
                <br>
                <button class="btn btn-stat" onclick="onNewsPerDayClick()">News per day</button>


            </div>
        </div>
    </div>
    <div id="news">

    </div>

    <div id="arrow-container">
        <img src="src/resources/css/img/left.png" onclick="onLeftArrowClick()" class="image-button">
        <img src="src/resources/css/img/right.png" onclick="onRightArrowClick()" class="image-button">
    </div>
</div>

<?php
ini_set("log_errors", 1);
ini_set("error_log", "src/logs/php-error.log");

?>
</body>


</html>
