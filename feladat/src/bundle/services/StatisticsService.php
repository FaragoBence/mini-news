<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 20.
 * Time: 9:43
 */

interface StatisticsService
{
    public function getTopTenMostUsedTags(): array;

    public function getNewsWithMostWords(): News;

    public function getAverageNewsLengthPerDay(): array;

    public function getNumOfNewsPerDay(): array;

}