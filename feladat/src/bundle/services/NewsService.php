<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 13:09
 */


interface NewsService
{
    public function addNews(string $title, string $content, array $tagTitles): void;

    public function readTenSortedByAsc(int $offset): array;

    public function readTenSortedByDesc(int $offset): array;
}