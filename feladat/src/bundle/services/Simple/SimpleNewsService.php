<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 15:37
 */


class SimpleNewsService implements NewsService
{
    private $newsDao;
    private $tagsDao;
    private $newsTagsDao;

    public function __construct(NewsDao $newsDao, TagsDao $tagsDao, NewsTagsDao $newsTagsDao)
    {
        $this->newsDao = $newsDao;
        $this->tagsDao = $tagsDao;
        $this->newsTagsDao = $newsTagsDao;
    }


    public function addNews(string $title, string $content, array $tagTitles): void
    {
        if (strlen($title) > 64) {
            throw new InvalidArgumentException("News title too long");
        }
        if ($this->newsDao->checkNewsExists($title)) {
            throw new NewsAlreadyExistsException("News with this title already exists");
        }
        $news = new News();
        $news->title = $title;
        $news->content = $content;
        $t = time();
        $news->timestamp = date("Y-m-d", $t);

        foreach ($tagTitles as $tagTitle) {

            if (strlen($tagTitle) > 32) {
                throw new InvalidArgumentException("Tag title too long");
            } else if ($this->tagsDao->checkTagExists($tagTitle)) {
                $tag = $this->tagsDao->findTagByTitle($tagTitle);
            } else {
                $tag = new Tag();
                $tag->title = $tagTitle;
                $this->tagsDao->insertTag($tag);
                $tag = $this->tagsDao->findTagByTitle($tagTitle);
            }

            $this->newsDao->insertNews($news);

            $news = $this->newsDao->findNewsByTitle($title);

            $this->newsTagsDao->insertNewsTags($tag->id, $news->id);
        }

    }

    public function readTenSortedByAsc(int $offset): array
    {
        if ($offset < 0) {
            throw new InvalidArgumentException("Offset can't be negative");
        }
        $newsArray = $this->newsDao->readTenSortedByTimestampAsc($offset);
        return $this->setTags($newsArray);
    }

    private function setTags(array $newsArray): array
    {
        foreach ($newsArray as $news) {
            $news->tags = $this->tagsDao->findTagsByNewsId($news->id);
        }
        return $newsArray;
    }

    public function readTenSortedByDesc(int $offset): array
    {
        if ($offset < 0) {
            throw new InvalidArgumentException("Offset can't be negative");
        }
        $newsArray = $this->newsDao->readTenSortedByTimestampDesc($offset);
        return $this->setTags($newsArray);
    }
}