<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 20.
 * Time: 9:45
 */


class SimpleStatisticsService implements StatisticsService
{
    private $statisticsDao;
    private $tagsDao;

    public function __construct(StatisticsDao $statisticsDao, TagsDao $tagsDao)
    {
        $this->statisticsDao = $statisticsDao;
        $this->tagsDao = $tagsDao;
    }

    public function getTopTenMostUsedTags(): array
    {
        return $this->statisticsDao->findTopTenMostUsedTags();
    }

    public function getNewsWithMostWords(): News
    {
        $news = $this->statisticsDao->findNewsWithMostWords();
        $news->tags = $this->tagsDao->findTagsByNewsId($news->id);
        return $news;
    }

    public function getAverageNewsLengthPerDay(): array
    {
        return $this->statisticsDao->findAverageNewsLengthPerDay();
    }

    public function getNumOfNewsPerDay(): array
    {
        return $this->statisticsDao->findNumOfNewsPerDay();
    }
}