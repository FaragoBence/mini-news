<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 13:11
 */


class DatabaseConfig
{

    public static function connect()
    {
        $db_connection = pg_connect("host=localhost dbname=news_portal user=postgres password=admin")
        or die('Could not connect to database' . pg_last_error());

        if ($db_connection) {
            return $db_connection;
        }

    }
}
