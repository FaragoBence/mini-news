<?php

class Loader
{
    const UNABLE_TO_LOAD = 'Unable to load class';
    protected static $dirs = array();
    protected static $registered = 0;

    /**
     * Initializes directories array
     *
     * @param array $dirs
     */
    public function __construct(array $dirs = array())
    {
        self::init($dirs);
    }

    /**
     * Adds a directory to the list of supported directories
     * Also registers "autoload" as an autoloading method
     *
     * @param array | string $dirs
     */
    public static function init($dirs = array())
    {
        if ($dirs) {
            self::addDirs($dirs);
        }
        if (self::$registered == 0) {
            spl_autoload_register(__CLASS__ . '::autoload');
            self::$registered++;
        }
    }

    /**
     * Adds directories to the existing array of directories
     *
     * @param array | string $dirs
     */
    public static function addDirs($dirs)
    {
        if (is_array($dirs)) {
            self::$dirs = array_merge(self::$dirs, $dirs);
        } else {
            self::$dirs[] = $dirs;
        }
    }

    /**
     * Locates a class file
     *
     * @param string $class
     * @return boolean
     * @throws Exception
     */
    public static function autoLoad($class): bool
    {
        $success = FALSE;

        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';

        foreach (self::$dirs as $start) {
            $file = $start . DIRECTORY_SEPARATOR . $fileName;
            if (self::loadFile($file)) {
                $success = TRUE;
                break;
            }
        }

        if (!$success) {
            if (!self::loadFile(__DIR__ . DIRECTORY_SEPARATOR . $fileName)) {
                throw new \Exception(self::UNABLE_TO_LOAD . ' ' . $class);
            }
        }
        return $success;
    }

    /**
     * Loads a file
     *
     * @param string $file
     * @return boolean
     */
    protected static function loadFile($file): bool
    {
        if (file_exists($file)) {
            require_once $file;
            return TRUE;
        }
        return FALSE;
    }

    public static function getDirectoryList(): array
    {
        $base = "../";
        $dirs = array();

        array_push($dirs, $base . "config", $base . "controllers", $base . "daos", $base . "daos/simple",
            $base . "services", $base . "services/simple", $base . "models", $base . "models/statistics",
            $base . "exceptions", $base . "routes");

        return $dirs;
    }
}