<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 21.
 * Time: 11:21
 */

class Logger
{
    public function errorLog(Exception $exception): void
    {
        $logMessage = "Error Message: ";
        $logMessage .= $exception->getMessage() . "\n";
        $logMessage .= "Stack Trace: ";
        $logMessage .= $exception->getTraceAsString() . "\n";
        $logMessage .= "File: ";
        $logMessage .= $exception->getFile() . "\n";
        $logMessage .= "IP Address: ";
        $logMessage .= $_SERVER['REMOTE_ADDR'] . "\n \n \n";
        error_log($logMessage, 3, "../../logs/php-error.log");
    }


}