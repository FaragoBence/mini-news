DROP TABLE IF EXISTS news_tags;
DROP TABLE IF EXISTS tags;
DROP TABLE IF EXISTS news;


CREATE TABLE news (
    id SERIAL PRIMARY KEY,
    title VARCHAR(64) UNIQUE NOT NULL,
    content TEXT NOT NULL,
    timestamp DATE NOT NULL
);

CREATE TABLE tags (
	id SERIAL PRIMARY KEY,
	tag_title VARCHAR(32) UNIQUE NOT NULL
);

CREATE TABLE news_tags (
	news_id INTEGER NOT NULL,
	tag_id INTEGER NOT NULL,
	FOREIGN KEY (news_id) REFERENCES news(id),
	FOREIGN KEY (tag_id) REFERENCES tags(id),
	PRIMARY KEY (news_id, tag_id)
);

INSERT INTO news ("title","content","timestamp") VALUES
('New panda baby', 'A new panda baby born in the zoo', '2018-08-10'), --1
('New programming language', 'A new programming language created by Google named GO is shining','2018-10-03'), --2
('Bruce Willis suddenly died', 'The actor is found by his family at Monday morning', '2018-10-01'), --3
('Petrol prices is growing', 'The petrol prices are growing because the new war in Saud-arabia', '2018-04-15'), --4
('Justin Bieber gives birth for twins', 'The singer after his successful operation gived birth for a cute boy and a girl', '2016-06-09'), --5
('The first hungarian airship landed on the mars', 'The first Hungarian airship named Puli succesfully landed on the moon surface on sunday', '2018-05-07'), --6
('A two headed dog born in Budapest', 'The two headed dogs eats twice as a normal dog', '2017-03-01'), --7
('Harry Potter fan uncovers bizarre Hogwarts plot hole','A Reddit user by the name steampunk_penguin_ revealed in a post that they don''t understand how all the students at the magical school afford to buy the parchment they do their work on.','2016-06-09'), --8
('Man survives bear attack and shark bite, in one year', 'Colorado man survives bear mauling and shark bite within the span of one year. KUSA''s Jennifer Meckles reports.','2016-01-12'), --9
('Heroic Teen Saves Choking Friend','Heroic Teen Saves Choking Friend','2018-08-02'), --10
('Jet-Powered Suit Sets New Fastest Speed as World Records Tumble','Missouri teen, who has Autism Spectrum Disorder, springs into action when a coin tossed on his school bus flies through the air and into his friend''s mouth, lodging in her throat. KSDK''s Jenna Barnes reports.','2014-05-13' --11
);

INSERT INTO tags ("tag_title") VALUES
('Animal'), --1
('Zoo'), --2
('Panda'), --3
('Actor'), --4
('Death'), --5
('Singer'), --6
('Birth'), --7
('Hungary'), --8
('Petrol'), --9
('Economy'), --10
('Harry Potter'), --11
('Theory'), --12
('Teen'), --13
('Hero'), --14
('Technology'), --15
('Record'), --16
('Programming'--17
);

INSERT INTO news_tags("news_id","tag_id") VALUES
(1,1),
(1,2),
(1,3),
(2,4),
(2,5),
(3,15),
(3,17),
(4,9),
(4,10),
(5,6),
(5,7),
(5,13),
(6,8),
(6,15),
(7,1),
(7,8),
(8,11),
(8,12),
(9,1),
(9,2),
(9,14),
(10,13),
(10,14),
(11,15),
(11,16);
