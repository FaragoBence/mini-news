<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 20.
 * Time: 14:14
 */


include_once "../config/Loader.php";

$dirs = Loader::getDirectoryList();

$load = new Loader($dirs);

$offset = $_GET["offset"];
$order = $_GET["order"];


$newsController = new NewsController();
$newsController->readNews($offset, $order);
