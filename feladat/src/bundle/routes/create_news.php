<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 20.
 * Time: 16:19
 */

include_once "../config/Loader.php";

$dirs = Loader::getDirectoryList();

$load = new Loader($dirs);


$newsController = new NewsController();

$data = json_decode(file_get_contents('php://input'), true);

$title = $data['title'];

$content = $data['content'];

$tags = array();

foreach ($data['tags'] as $tagNames) {
    array_push($tags, $tagNames['title']);
}

$newsController->addNews($title, $content, $tags);