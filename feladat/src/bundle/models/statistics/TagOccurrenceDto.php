<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 20.
 * Time: 8:49
 */


class TagOccurrenceDto extends BaseStatisticModel
{
    protected $tag;
    protected $occurrence;

    public function __construct(Tag $tag, int $occurrence)
    {
        $this->tag = $tag;
        $this->occurrence = $occurrence;
    }

    public static function encodeListToJson(array $data): array
    {
        $list = array();
        foreach ($data as $tagOccurrence) {
            $list[] = self::encodeToJson($tagOccurrence);
        }
        return $list;
    }

    public static function encodeToJson(BaseStatisticModel $tagOccurrenceDto): array
    {
        $tag = Tag::encodeToJson($tagOccurrenceDto->tag);
        return array('numOfOccurrences' => $tagOccurrenceDto->occurrence, 'tag' => $tag);
    }

}
