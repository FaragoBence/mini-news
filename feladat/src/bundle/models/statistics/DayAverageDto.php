<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 20.
 * Time: 8:49
 */


class DayAverageDto extends BaseStatisticModel
{
    protected $average;
    protected $date;

    public function __construct(float $average, String $date)
    {
        $this->average = $average;
        $this->date = $date;
    }

    public static function encodeListToJson(array $data)
    {
        $list = array();
        foreach ($data as $dayAverage) {
            $list[] = self::encodeToJson($dayAverage);
        }
        return $list;
    }

    public static function encodeToJson(BaseStatisticModel $dayAverage)
    {
        return array('average' => $dayAverage->average, 'date' => $dayAverage->date);
    }
}
