<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 20.
 * Time: 8:56
 */


abstract class BaseStatisticModel
{
    public abstract static function encodeToJson(BaseStatisticModel $statisticModel);

    public function __get($property)
    {
        return $this->$property;
    }
}
