<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 20.
 * Time: 8:49
 */


class DayCountDto extends BaseStatisticModel
{
    protected $count;
    protected $date;

    public function __construct(int $count, String $date)
    {
        $this->count = $count;
        $this->date = $date;
    }

    public static function encodeListToJson(array $data)
    {
        $list = array();
        foreach ($data as $dayCount) {
            $list[] = self::encodeToJson($dayCount);
        }
        return $list;
    }

    public static function encodeToJson(BaseStatisticModel $dayCountDto)
    {
        return array('count' => $dayCountDto->count, 'date' => $dayCountDto->date);
    }

}
