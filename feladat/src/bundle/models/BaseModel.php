<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 21.
 * Time: 9:13
 */

abstract class BaseModel
{

    public abstract static function encodeToJson(BaseModel $baseModel);


    public function __get($property)
    {
        return $this->$property;
    }

    public function __set($property, $value)
    {
        $this->$property = $value;
    }
}