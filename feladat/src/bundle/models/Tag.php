<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 11:30
 */


class Tag extends BaseModel
{
    protected $id;
    protected $title;

    public static function encodeToJson(BaseModel $tag): array
    {
        return array('id' => $tag->id, 'title' => $tag->title);
    }

    public function fetch($data)
    {
        $this->id = $data->id;
        $this->title = $data->tag_title;
    }
}
