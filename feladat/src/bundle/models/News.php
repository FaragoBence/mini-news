<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 11:31
 */

class News extends BaseModel
{
    protected $id;
    protected $title;
    protected $content;
    protected $tags;
    protected $timestamp;

    public static function encodeListToJson(array $data): array
    {
        $list = array();
        foreach ($data as $news) {

            $list[] = News::encodeToJson($news);
        }
        return $list;
    }

    public static function encodeToJson(BaseModel $news): array
    {
        $tagList = null;
        foreach ($news->tags as $tag) {
            $tagList[] = Tag::encodeToJson($tag);
        }
        return array('id' => $news->id, 'content' => $news->content,
            'timestamp' => $news->timestamp, 'title' => $news->title, 'tags' => $tagList);
    }

    public function fetch($data)
    {
        $this->id = $data->id;
        $this->title = $data->title;
        $this->content = $data->content;
        $this->timestamp = $data->timestamp;
    }


}
