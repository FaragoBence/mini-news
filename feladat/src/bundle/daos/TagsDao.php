<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 13:24
 */


interface TagsDao
{
    public function insertTag(Tag $tag): void;

    public function findTagsByNewsId(int $id): array;

    public function checkTagExists(string $title): bool;

    public function findTagByTitle(string $title): Tag;

}