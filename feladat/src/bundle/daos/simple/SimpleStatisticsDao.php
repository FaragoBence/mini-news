<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 20.
 * Time: 8:47
 */


class SimpleStatisticsDao implements StatisticsDao
{

    private $db_connection;

    public function __construct()
    {
        $this->db_connection = DatabaseConfig::connect();
    }


    public function findTopTenMostUsedTags(): array
    {
        $tagOccurrences = array();
        $select = "SELECT t.id as id, t.tag_title as tag_title, COUNT(nt.news_id) as count " .
            "FROM tags t JOIN news_tags nt ON nt.tag_id = t.id GROUP BY t.id " .
            "ORDER BY count desc LIMIT 10;";
        $result = pg_query($this->db_connection, $select);
        while ($data = pg_fetch_object($result)) {
            array_push($tagOccurrences, $this->createTagoccurrence($data));
        }
        return $tagOccurrences;
    }

    private function createTagOccurrence($data)
    {
        $tag = new Tag();
        $tag->fetch($data);
        $count = $data->count;
        return new TagOccurrenceDto($tag, $count);
    }

    public function findNewsWithMostWords(): News
    {
        $news = new News();
        $select = "SELECT n.id,n.title,n.content,n.timestamp," .
            "MAX(LENGTH(n.content) - LENGTH(REPLACE(n.content, ' ', '')) + 1) as countwords FROM news n " .
            "GROUP by n.id ORDER BY countwords DESC LIMIT 1;";

        $result = pg_query($this->db_connection, $select);
        if ($data = pg_fetch_object($result)) {
            $news->fetch($data);
        }
        return $news;

    }

    public function findAverageNewsLengthPerDay(): array
    {
        $dayAverage = array();
        $select = "SELECT AVG(LENGTH(content)) as avg, timestamp FROM news " .
            "GROUP BY timestamp ORDER BY timestamp DESC;";
        $result = pg_query($this->db_connection, $select);
        while ($data = pg_fetch_object($result)) {
            array_push($dayAverage, $this->createDayAverage($data));
        }
        return $dayAverage;
    }

    private function createDayAverage($data)
    {
        $average = $data->avg;
        $timestamp = $data->timestamp;
        return new DayAverageDto($average, $timestamp);
    }

    public function findNumOfNewsPerDay(): array
    {
        $dayCount = array();
        $select = "SELECT timestamp, COUNT(id) as count FROM news " .
            "GROUP BY(timestamp) ORDER BY timestamp DESC;";
        $result = pg_query($this->db_connection, $select);
        while ($data = pg_fetch_object($result)) {
            array_push($dayCount, $this->createDayCount($data));
        }
        return $dayCount;
    }

    private function createDayCount($data)
    {
        $count = $data->count;
        $timestamp = $data->timestamp;
        return new DayCountDto($count, $timestamp);
    }

    public function __destruct()
    {
        pg_close($this->db_connection);
    }

}