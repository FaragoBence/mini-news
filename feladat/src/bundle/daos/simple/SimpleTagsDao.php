<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 15:10
 */


class SimpleTagsDao implements TagsDao
{
    private $db_connection;

    public function __construct()
    {
        $this->db_connection = DatabaseConfig::connect();
    }

    public function insertTag(Tag $tag): void
    {
        $insert = "INSERT INTO tags(tag_title) VALUES('$tag->title')";
        pg_query($this->db_connection, $insert);
    }

    public function findTagsByNewsId(int $id): array
    {
        $tags = array();
        $select = "SELECT t.id, t.tag_title " .
            "FROM tags t JOIN news_tags nt ON nt.tag_id = t.id " .
            "WHERE nt.news_id = " . $id;
        $result = pg_query($this->db_connection, $select);
        while ($data = pg_fetch_object($result)) {
            $tag = new Tag();
            $tag->fetch($data);
            array_push($tags, $tag);
        }
        return $tags;
    }

    public function checkTagExists(string $title): bool
    {
        $result = $this->findByTitle($title);
        if (!$result) {
            echo "query did not execute";
        }
        return pg_num_rows($result) != 0;
    }

    private function findByTitle(String $title)
    {
        $select = 'SELECT * FROM tags WHERE tag_title = $1';
        $result = pg_query_params($this->db_connection, $select, array($title));
        return $result;
    }

    public function findTagByTitle(string $title): Tag
    {
        $tag = new Tag();
        $result = $this->findByTitle($title);
        if ($data = pg_fetch_object($result)) {
            $tag = new Tag();
            $tag->fetch($data);
        }
        return $tag;
    }


}