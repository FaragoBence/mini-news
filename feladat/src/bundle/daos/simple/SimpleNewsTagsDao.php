<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 15:21
 */


class SimpleNewsTagsDao implements NewsTagsDao
{

    private $db_connection;

    public function __construct()
    {
        $this->db_connection = DatabaseConfig::connect();
    }


    public function insertNewsTags(int $tag_id, int $news_id): void
    {
        $insert = "INSERT INTO news_tags(news_id, tag_id) VALUES ('$news_id', '$tag_id');";
        pg_query($this->db_connection, $insert);
    }
}