<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 13:17
 */


class SimpleNewsDao implements NewsDao
{
    private $db_connection;

    public function __construct()
    {
        $this->db_connection = DatabaseConfig::connect();
    }

    public function insertNews(News $news): void
    {
        $insert = "INSERT INTO news(title, content, timestamp) VALUES ('$news->title', '$news->content', '$news->timestamp');";
        pg_query($this->db_connection, $insert);
    }

    public function checkNewsExists(String $title): bool
    {
        $result = $this->findByTitle($title);
        if (!$result) {
            echo "query did not execute";
        }
        return pg_num_rows($result) != 0;
    }

    private function findByTitle(String $title)
    {
        $select = 'SELECT * FROM news WHERE title = $1 ';
        $result = pg_query_params($this->db_connection, $select, array($title));
        return $result;
    }

    public function readTenSortedByTimestampAsc(int $offset): array
    {
        $newsArray = array();
        $result = pg_query($this->db_connection, "SELECT * FROM news ORDER BY timestamp ASC, id DESC LIMIT 10 OFFSET " . $offset);
        while ($data = pg_fetch_object($result)) {
            $news = new News();
            $news->fetch($data);
            array_push($newsArray, $news);
        }
        return $newsArray;
    }

    public function readTenSortedByTimestampDesc(int $offset): array
    {
        $newsArray = array();
        $result = pg_query($this->db_connection, "SELECT * FROM news ORDER BY timestamp DESC, id DESC LIMIT 10 OFFSET " . $offset);
        while ($data = pg_fetch_object($result)) {
            $news = new News();
            $news->fetch($data);
            array_push($newsArray, $news);
        }
        return $newsArray;
    }

    public function findNewsByTitle(String $title): News
    {
        $news = new News();
        $result = $this->findByTitle($title);
        if ($data = pg_fetch_object($result)) {
            $news->fetch($data);
        }
        return $news;
    }
}