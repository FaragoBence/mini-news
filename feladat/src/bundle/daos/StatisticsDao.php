<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 13:28
 */


interface StatisticsDao
{
    public function findTopTenMostUsedTags(): array;

    public function findNewsWithMostWords(): News;

    public function findAverageNewsLengthPerDay(): array;

    public function findNumOfNewsPerDay(): array;


}