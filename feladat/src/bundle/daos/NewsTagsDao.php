<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 13:57
 */


interface NewsTagsDao
{
    public function insertNewsTags(int $tag_id, int $news_id);
}