<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 13:19
 */


interface NewsDao
{
    public function insertNews(News $news): void;

    public function checkNewsExists(String $title): bool;

    public function findNewsByTitle(String $title): News;

    public function readTenSortedByTimestampAsc(int $offset): array;

    public function readTenSortedByTimestampDesc(int $offset): array;


}