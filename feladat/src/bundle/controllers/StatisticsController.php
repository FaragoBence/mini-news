<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 21.
 * Time: 9:07
 */

class StatisticsController
{

    private $statisticsService;

    public function __construct()
    {
        $statisticsDao = new SimpleStatisticsDao();
        $tagsDao = new SimpleTagsDao();
        $this->statisticsService = new SimpleStatisticsService($statisticsDao, $tagsDao);
    }

    public function topTenMostUsedTags()
    {
        $data = $this->statisticsService->getTopTenMostUsedTags();
        echo json_encode(TagOccurrenceDto::encodeListToJson($data));
    }

    public function newsPerDay()
    {
        $data = $this->statisticsService->getNumOfNewsPerDay();
        echo json_encode(DayCountDto::encodeListToJson($data));
    }

    public function newsLengthPerDay()
    {
        $data = $this->statisticsService->getAverageNewsLengthPerDay();
        echo json_encode(DayAverageDto::encodeListToJson($data));
    }

    public function newsWithMostWords()
    {
        $data = $this->statisticsService->getNewsWithMostWords();
        echo json_encode(News::encodeToJson($data));
    }


}