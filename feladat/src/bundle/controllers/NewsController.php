<?php


/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018. 11. 19.
 * Time: 11:47
 */


class NewsController
{
    private $simpleNewsService;
    private $logger;

    public function __construct()
    {
        $simpleNewsDao = new SimpleNewsDao();
        $simpleTagsDao = new SimpleTagsDao();
        $simpleNewsTagsDao = new SimpleNewsTagsDao();
        $this->simpleNewsService = new SimpleNewsService($simpleNewsDao, $simpleTagsDao, $simpleNewsTagsDao);
        $this->logger = new Logger();
    }

    public function addNews(string $title, string $content, array $tags)
    {

        try {
            $this->simpleNewsService->addNews($title, $content, $tags);
        } catch (InvalidArgumentException $e) {
            $this->logger->errorLog($e);
            http_response_code(400);
            die(json_encode(array('message' => $e->getMessage())));
        } catch (NewsAlreadyExistsException $e) {
            $this->logger->errorLog($e);
            http_response_code(409);
            die(json_encode(array('message' => $e->getMessage())));
        }

        $this->readNews(0, "desc");
    }

    public function readNews(int $offset, string $order)
    {

        try {
            if ($order == "asc") {
                $data = $this->simpleNewsService->readTenSortedByAsc($offset);
            } else {
                $data = $this->simpleNewsService->readTenSortedByDesc($offset);
            }

        } catch (InvalidArgumentException $e) {
            $this->logger->errorLog($e);
            http_response_code(400);
            die(json_encode(array('message' => $e->getMessage())));
        }
        header('Content-type: application/json');
        $list = News::encodeListToJson($data);
        echo json_encode($list);
    }
}
