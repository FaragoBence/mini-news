let offset = 0;
let order = "desc";

function onMainPageLoad() {
    clearMessages();
    document.getElementById("header-text").innerText = "Mini-News";
    getNewsWithOffset(offset);
    if (arrowDivEl.classList.contains("hidden")) {
        arrowDivEl.classList.remove("hidden");
    }
}

function getNewsWithOffset(offset) {
    const params = new URLSearchParams();
    params.append('offset', offset.toString());
    params.append('order', order);
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onGetNewsResponse);
    xhr.addEventListener('error', onNetworkError);
    xhr.open('GET', BASE + 'get_news.php?' + params.toString());
    xhr.send();
}

function onGetNewsResponse() {
    if (this.status === OK) {
        const news = JSON.parse(this.responseText);
        if (news.length == 0) {
            offset = offset - 10;
            getNewsWithOffset(offset);
        }
        createNewsDiv(news);
    } else {
        onMessageResponse(mainDivEl, this);
    }
}


function createNewsDiv(news) {
    removeAllChildren(newsDivEl);
    clearMessages();
    for (let i = 0; i < news.length; i++) {

        let headerTrEl = document.createElement('tr');
        headerTrEl.setAttribute("class", "news-header");

        let titleTdEl = document.createElement('td');
        titleTdEl.setAttribute("class", "title");
        titleTdEl.innerText = news[i].title;

        let timeTdEl = document.createElement("td");
        timeTdEl.setAttribute("class", "time");
        timeTdEl.innerText = news[i].timestamp;

        headerTrEl.appendChild(titleTdEl);
        headerTrEl.appendChild(timeTdEl);

        let newsContentTrEl = document.createElement("tr");
        newsContentTrEl.setAttribute("class", "news-main");

        let newsContentTdEl = document.createElement("td");
        newsContentTdEl.setAttribute("class", "content");
        newsContentTdEl.innerText = news[i].content;
        newsContentTdEl.colSpan = 2;

        newsContentTrEl.appendChild(newsContentTdEl);

        let newsTagsTrEl = document.createElement("tr");
        newsTagsTrEl.setAttribute("class", "tags-tr");

        let newsTagsTdEl = document.createElement("td");
        newsTagsTdEl.setAttribute("class", "tags-td");
        newsTagsTdEl.colSpan = 2;


        for (let k = 0; k < news[i].tags.length; k++) {
            let spanTagEl = document.createElement("span");
            spanTagEl.setAttribute("class", "tag");
            spanTagEl.innerText = "#" + news[i].tags[k].title + " ";
            newsTagsTdEl.appendChild(spanTagEl);
        }

        newsTagsTrEl.appendChild(newsTagsTdEl);


        let tableEl = document.createElement("table");
        tableEl.setAttribute("class", "news-table");

        tableEl.appendChild(headerTrEl);
        tableEl.appendChild(newsContentTrEl);
        tableEl.appendChild(newsTagsTrEl);


        newsDivEl.appendChild(tableEl);
    }
}

function createBackButton() {
    const backButtonEl = document.createElement("button");
    backButtonEl.innerText = "Back";
    backButtonEl.classList.add("btn");
    backButtonEl.classList.add("btn-back");
    backButtonEl.addEventListener("click", onBackButtonClick);
    newsDivEl.appendChild(backButtonEl);
}

function onBackButtonClick() {
    onMainPageLoad();
}

function onRightArrowClick() {
    offset = offset + 10;
    getNewsWithOffset(offset);
}

function onLeftArrowClick() {
    if (offset - 10 >= 0) {
        offset = offset - 10;
        getNewsWithOffset(offset);
    }
}
