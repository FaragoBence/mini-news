const OK = 200;
const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const CONFLICT = 409;


const BASE = "src/bundle/routes/";


let newsDivEl;
let addNewsDivEl;
let mainDivEl;
let arrowDivEl;

function newInfo(targetEl, message) {
    newMessage(targetEl, 'info', message);
}

function newError(targetEl, message) {
    newMessage(targetEl, 'error', message);
}

function newMessage(targetEl, cssClass, message) {
    clearMessages();

    const pEl = document.createElement('h3');
    pEl.classList.add('message');
    pEl.classList.add(cssClass);
    pEl.textContent = message;
    const firstChild = targetEl.firstChild;
    targetEl.insertBefore(pEl, firstChild);
}

function clearMessages() {
    const messageEls = document.getElementsByClassName('message');
    for (let i = 0; i < messageEls.length; i++) {
        const messageEl = messageEls[i];
        messageEl.remove();
    }
}

function showContents(ids) {
    const contentEls = document.getElementsByClassName('content-div');
    for (let i = 0; i < contentEls.length; i++) {
        const contentEl = contentEls[i];
        if (ids.includes(contentEl.id)) {
            contentEl.classList.remove('hidden');
        } else {
            contentEl.classList.add('hidden');
        }
    }
}

function removeAllChildren(el) {
    while (el.firstChild) {
        el.removeChild(el.firstChild);
    }
}

function onNetworkError(response) {
    document.body.remove();
    const bodyEl = document.createElement('body');
    document.appendChild(bodyEl);
    newError(bodyEl, 'Network error, please try reloading the page');
}

function onMessageResponse(targetEl, xhr) {
    const json = JSON.parse(xhr.responseText);
    if (xhr.status === BAD_REQUEST || xhr.status === CONFLICT || xhr.status === NOT_FOUND) {
        newError(targetEl, `Error: ${json.message}`);
    } else if (xhr.status === OK) {
        newInfo(targetEl, json.message);
    } else {
        newError(targetEl, `Unknown error: ${json.message}`);
    }
}


function onLoad() {
    newsDivEl = document.getElementById('news');
    addNewsDivEl = document.getElementById('add-news-div');
    mainDivEl = document.getElementById('main-div');
    arrowDivEl = document.getElementById('arrow-container');
    onMainPageLoad();
}

document.addEventListener('DOMContentLoaded', onLoad);
