function onOrderByDateAscClick() {
    order = "asc";
    const params = new URLSearchParams();
    params.append('offset', offset.toString());
    params.append('order', order);
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onOrderByDateAscResponse);
    xhr.addEventListener('error', onNetworkError);
    xhr.open('GET', BASE + 'get_news.php?' + params.toString());
    xhr.send();
}

function onOrderByDateAscResponse() {
    if (this.status === OK) {
        const news = JSON.parse(this.responseText);
        if (news.length == 0) {
            offset = offset - 10;
            onOrderByDateAscClick(offset);
        }
        createNewsDiv(news);
    } else {
        onMessageResponse(mainDivEl, this);
    }
}

function onOrderByDateDescClick() {
    order = "desc";
    onMainPageLoad();
}


function onNewsWithMostWordsClick() {
    hideArrowDiv();

    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onNewsWithMostWordsResponse);
    xhr.addEventListener('error', onNetworkError);
    xhr.open('GET', BASE + 'news_with_most_words.php');
    xhr.send();
}

function onNewsWithMostWordsResponse() {
    if (this.status === OK) {
        document.getElementById("header-text").innerText = "News with the most word";
        const singleNews = JSON.parse(this.responseText);
        const news = [singleNews];
        createNewsDiv(news);
        createBackButton();

    } else {
        onMessageResponse(mainDivEl, this);
    }
}

function onMostUsedTagsClick() {

    hideArrowDiv();

    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onMostUsedTagsResponse);
    xhr.addEventListener('error', onNetworkError);
    xhr.open('GET', BASE + 'tag_occurrences.php');
    xhr.send();
}

function onMostUsedTagsResponse() {
    if (this.status === OK) {
        document.getElementById("header-text").innerText = "Top 10 most used tags";
        const tagOccurrenceDtos = JSON.parse(this.responseText);
        clearMessages();
        appendTags(tagOccurrenceDtos);
        createBackButton();
    } else {
        onMessageResponse(newsDivEl, this);
    }
}

function onNewsPerDayClick() {
    hideArrowDiv();

    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onNewsPerDayResponse);
    xhr.addEventListener('error', onNetworkError);
    xhr.open('GET', BASE + 'num_of_news_per_day.php');
    xhr.send();
}

function onNewsPerDayResponse() {
    if (this.status === OK) {
        document.getElementById("header-text").innerText = "Number of news per day";
        const DayNumberDtos = JSON.parse(this.responseText);
        clearMessages();
        appendDayNumberDtos(DayNumberDtos, "Count");
        createBackButton();
    } else {
        onMessageResponse(newsDivEl, this);
    }
}

function onAverageNewsLengthClick() {
    hideArrowDiv();

    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onAverageNewsLengthResponse);
    xhr.addEventListener('error', onNetworkError);
    xhr.open('GET', BASE + 'length_of_news_per_day.php');
    xhr.send();
}

function onAverageNewsLengthResponse() {
    if (this.status === OK) {
        document.getElementById("header-text").innerText = "Average news length per day";
        const DayNumberDtos = JSON.parse(this.responseText);
        clearMessages();
        appendDayNumberDtos(DayNumberDtos, "Average");
        createBackButton();
    } else {
        onMessageResponse(newsDivEl, this);
    }
}


function appendTag(tableEl, tagOccurrenceDto) {
    const occurrenceTdEl = document.createElement('td');
    occurrenceTdEl.textContent = tagOccurrenceDto.numOfOccurrences;

    const titleTdEl = document.createElement('td');
    titleTdEl.textContent = tagOccurrenceDto.tag.title;

    const trEl = document.createElement('tr');
    trEl.appendChild(occurrenceTdEl);
    trEl.appendChild(titleTdEl);
    tableEl.appendChild(trEl);
    return tableEl;
}

function appendTags(tagOccurrenceDtos) {
    removeAllChildren(newsDivEl);
    let tableEl = document.createElement("table");
    tableEl.setAttribute("class", "content-table");

    tableEl.appendChild(createTagsTableHeading());

    for (let i = 0; i < tagOccurrenceDtos.length; i++) {
        const tagOccurrenceDto = tagOccurrenceDtos[i];
        tableEl = appendTag(tableEl, tagOccurrenceDto);
    }
    newsDivEl.appendChild(tableEl);
}


function createTagsTableHeading() {
    const occurrenceThEl = document.createElement('th');
    occurrenceThEl.textContent = "Occurrence";

    const titleThEl = document.createElement('th');
    titleThEl.textContent = "Title";

    const thTrEl = document.createElement('tr');
    thTrEl.appendChild(occurrenceThEl);
    thTrEl.appendChild(titleThEl);
    return thTrEl;
}


function appendDayNumberDto(tableEl, DayNumberDto, type) {
    const dayTdEl = document.createElement('td');
    dayTdEl.textContent = DayNumberDto.date;

    const numberTdEl = document.createElement('td');
    if (type === "Count") {
        numberTdEl.textContent = DayNumberDto.count;
    } else if (type === "Average") {
        numberTdEl.textContent = DayNumberDto.average.toFixed(1);
    }

    const trEl = document.createElement('tr');
    trEl.appendChild(dayTdEl);
    trEl.appendChild(numberTdEl);
    tableEl.appendChild(trEl);
    return tableEl;
}

function appendDayNumberDtos(DayNumberDtos, type) {
    removeAllChildren(newsDivEl);
    let tableEl = document.createElement("table");
    tableEl.setAttribute("class", "content-table");

    tableEl.appendChild(createDayTableHeading(type));

    for (let i = 0; i < DayNumberDtos.length; i++) {
        const DayNumberDto = DayNumberDtos[i];
        tableEl = appendDayNumberDto(tableEl, DayNumberDto, type);
    }
    newsDivEl.appendChild(tableEl);
}

function createDayTableHeading(type) {
    const dayThEl = document.createElement('th');
    dayThEl.textContent = "Day";

    const typeThEl = document.createElement('th');
    typeThEl.textContent = type;

    const thTrEl = document.createElement('tr');
    thTrEl.appendChild(dayThEl);
    thTrEl.appendChild(typeThEl);
    return thTrEl;
}

function hideArrowDiv() {
    if (!arrowDivEl.classList.contains("hidden")) {
        arrowDivEl.classList.add("hidden");
    }
}

