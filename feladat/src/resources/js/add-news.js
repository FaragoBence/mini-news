function onCreateButtonClick() {
    clearMessages();

    const title = document.getElementById("title").value;
    const content = document.getElementById("content").value;
    let tags = document.getElementById("tags").value.split(' ');


    if (title === "") {
        newError(newsDivEl, "Enter a title");
        return;
    }
    if (title.length > 64) {
        newError(newsDivEl, "Title is too long");
        return;
    }
    if (content === "") {
        newError(newsDivEl, "Enter content for the news");
        return;
    }

    //Create set for get rid of multiplicand tags
    let set = new Set(tags);
    tags = Array.from(set);


    let jsonObj = {};
    jsonObj.title = title;
    jsonObj.content = content;
    jsonObj.tags = [];


    tags.map(function (tag) {
        jsonObj.tags.push({
            "title": tag
        });
    });
    const jsonString = JSON.stringify(jsonObj);
    console.log(jsonObj);
    console.log(jsonString);

    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', onCreateNewsResponse);
    xhr.addEventListener('error', onNetworkError);
    xhr.open('POST', BASE + 'create_news.php');
    xhr.send(jsonString);
}

function onCreateNewsResponse() {
    if (this.status === OK) {
        const news = JSON.parse(this.responseText);
        clearFields();
        createNewsDiv(news);
    } else {
        onMessageResponse(mainDivEl, this);
    }
}

function clearFields() {
    document.getElementById("title").value = "";
    document.getElementById("content").value = "";
    document.getElementById("tags").value = "";
}